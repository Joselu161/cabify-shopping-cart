export const applyDiscountTwoPerOne = (amount: number, price: number) =>
  Math.round(amount / 2) * price;
export const applyDiscountThreeUnits = (amount: number, price: number) =>
  amount > 2 ? amount * price * 0.95 : amount * price;
