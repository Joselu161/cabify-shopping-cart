import { Inject, Injectable } from '@angular/core';

import type { AvailableProducts, DiscountConfig } from 'business';
import { Checkout } from 'business';

@Injectable({
  providedIn: 'root',
})
export class CheckoutCartService extends Checkout {
  public constructor(
    @Inject('discountConfig') discountConfig: DiscountConfig[],
    @Inject('products') availableProducts: AvailableProducts
  ) {
    super(discountConfig, availableProducts);
  }
}
