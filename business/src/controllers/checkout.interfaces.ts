import type { ProductCode, ProductItem } from "../models";

export type AvailableProducts = Map<ProductCode, ProductItem>;

export type DiscountFunction = (amount: number, price: number) => number;

export enum ChangeQuantityAction {
  INCREASE,
  DECREASE,
  CUSTOM,
}

export interface DiscountConfig {
  name: string;
  applyOver: ProductCode;
  discountFuntion: DiscountFunction;
}

export interface Discount {
  name: string;
  discountAmount: number;
}
