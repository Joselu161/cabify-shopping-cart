import type { ProductItem } from "..";

/**
 * Component ProductsTable. It controls the visualization of the table and display each
 * product with their data
 */
export interface ProductsTable {
  products: ProductItem[];
}
