import { NgModule } from '@angular/core';
import type { Routes } from '@angular/router';
import { RouterModule } from '@angular/router';
import { CartOverviewComponent } from './screens/cart-overview/cart-overview.component';

const routes: Routes = [
  { path: '', redirectTo: 'cart-overview', pathMatch: 'full' },
  { path: 'cart-overview', component: CartOverviewComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
