export type ProductCode = "TSHIRT" | "CAP" | "MUG";

export interface ProductDetail {
  code: ProductCode;
  imgFileName: string;
  name: string;
  price: number;
}

export interface ProductItem extends ProductDetail {
  quantity?: number;
  totalItemPrice?: number;
}

export class Product implements ProductItem {
  constructor(
    public code: ProductCode,
    public imgFileName: string,
    public name: string,
    public price: number
  ) {}
}
