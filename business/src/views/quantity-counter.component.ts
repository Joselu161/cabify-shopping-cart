import type { ChangeQuantityAction } from "../controllers/checkout.interfaces";

/**
 * Component which handle actions to change the quantity of a product
 */
export interface QuantityCounter {
  changeAction: typeof ChangeQuantityAction;
  quantity: number;
  handleQuantity(action: ChangeQuantityAction): void;
}
