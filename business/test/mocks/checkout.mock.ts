import type {
  AvailableProducts,
  DiscountConfig,
} from "../../src/controllers/checkout.interfaces";
import type { ProductCode, ProductItem } from "../../src/models/product-item";
import { Product } from "../../src/models/product-item";
import {
  applyDiscountThreeUnits,
  applyDiscountTwoPerOne,
} from "../../src/utils/utils";

export const MOCK_AVAILABLE_PRODUCTS: AvailableProducts = new Map<
  ProductCode,
  ProductItem
>([
  ["TSHIRT", new Product("TSHIRT", "shirt.png", "Cabify T-Shirt", 20)],
  ["MUG", new Product("MUG", "mug.png", "Cafify Coffee Mug", 5)],
  ["CAP", new Product("CAP", "cap.png", "Cabify Cap", 10)],
]);

export const MOCK_DISCOUNTS_TWO_PER_ONE_MUG: DiscountConfig[] = [
  {
    name: "2x1 Mug Offer",
    applyOver: "MUG",
    discountFuntion: applyDiscountTwoPerOne,
  },
];

export const MOCK_DISCOUNTS_THREE_UNITS_CAP_TWO_PER_ONE_TSHIRT: DiscountConfig[] =
  [
    {
      name: "x3 Cap offer",
      applyOver: "CAP",
      discountFuntion: applyDiscountThreeUnits,
    },
    {
      name: "2x1 TShirt Offer",
      applyOver: "TSHIRT",
      discountFuntion: applyDiscountTwoPerOne,
    },
  ];

export const MOCK_CUSTOM_DISCOUNT: DiscountConfig[] = [
  {
    name: "20% TShirt CUSTOM JL",
    applyOver: "TSHIRT",
    discountFuntion: (amount: number, price: number) => {
      return amount * price * 0.8;
    },
  },
];
