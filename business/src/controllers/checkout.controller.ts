import type { ProductCode, ProductItem } from "../models/product-item";
import type {
  AvailableProducts,
  DiscountConfig,
  Discount,
} from "./checkout.interfaces";
import { ChangeQuantityAction } from "./checkout.interfaces";
import {
  changeQuantityOfProduct,
  removeProduct,
  updatePrice,
} from "./checkout.utils";

export class Checkout {
  public constructor(
    public discountConfig: DiscountConfig[],
    public availableProducts: AvailableProducts
  ) {
    this.setUpAvailableDiscounts(discountConfig);
  }

  private _cart: ProductItem[] = [];
  private _discountsEnabled: Discount[];

  public getCart(): ProductItem[] {
    return this._cart;
  }

  public setCart(newCart: ProductItem[]): void {
    this._cart = newCart.map(updatePrice);
  }

  public getDiscounts(): Discount[] {
    return this._discountsEnabled;
  }

  private setUpAvailableDiscounts(discounts: DiscountConfig[]): void {
    this._discountsEnabled = discounts.map((discount: DiscountConfig) => {
      return { name: discount.name, discountAmount: 0 };
    });
  }

  private handleDiscounts(): void {
    this._discountsEnabled = this.discountConfig.map(
      (discount: DiscountConfig) => {
        const { applyOver, discountFuntion } = discount;

        const cartItem = this.getCart().find(
          (cartItem: ProductItem) => cartItem.code === applyOver
        );

        if (cartItem) {
          const { quantity, price } = cartItem;
          const basePrice = quantity * price;
          return {
            name: discount.name,
            discountAmount: basePrice - discountFuntion(quantity, price),
          };
        }

        return { name: discount.name, discountAmount: 0 };
      }
    );
  }

  private createProduct(productCode: ProductCode): ProductItem {
    return this.availableProducts.get(productCode);
  }

  public scan(productCode: ProductCode): Checkout {
    if (this.cartHasItem(productCode)) {
      this.setCart(
        changeQuantityOfProduct(
          this.getCart(),
          productCode,
          ChangeQuantityAction.INCREASE
        )
      );
    } else {
      this.setCart([
        ...this.getCart(),
        { ...this.createProduct(productCode), quantity: 1 },
      ]);
    }

    this.handleDiscounts();
    return this;
  }

  public removeItem(productCode: ProductCode): Checkout {
    if (this.getQuantityOfProduct(productCode) > 1) {
      this.setCart(
        changeQuantityOfProduct(
          this.getCart(),
          productCode,
          ChangeQuantityAction.DECREASE
        )
      );
    } else {
      this.setCart(removeProduct(this.getCart(), productCode));
    }

    this.handleDiscounts();
    return this;
  }

  public changeQuantity(productCode: ProductCode, newQuantity: number) {
    if (this.cartHasItem(productCode)) {
      if (newQuantity > 0) {
        this.setCart(
          changeQuantityOfProduct(
            this.getCart(),
            productCode,
            ChangeQuantityAction.CUSTOM,
            newQuantity
          )
        );
      } else {
        this.setCart(removeProduct(this.getCart(), productCode));
      }
    } else {
      this.setCart([
        ...this.getCart(),
        { ...this.createProduct(productCode), quantity: newQuantity },
      ]);
    }

    this.handleDiscounts();
  }

  public getQuantityOfProduct(productCode: ProductCode): number {
    if (this.cartHasItem(productCode)) {
      return this.getProductCartData(productCode).quantity;
    }
    return 0;
  }

  public getTotalAmountOfProducts(): number {
    return this.getCart().reduce((total: number, cartItem: ProductItem) => {
      return total + cartItem.quantity;
    }, 0);
  }

  public getTotalBasePriceOfProducts(): number {
    return this.getCart().reduce((total: number, cartItem: ProductItem) => {
      return total + cartItem.totalItemPrice;
    }, 0);
  }

  public getProductCartData(productCode: ProductCode): ProductItem | undefined {
    return this.getCart().filter(
      (product: ProductItem) => product.code === productCode
    )[0];
  }

  private cartHasItem(code: ProductCode): boolean {
    return this.getCart().some(
      (cartItem: ProductItem) => cartItem.code === code
    );
  }

  public total(): number {
    const basePrice = this.getTotalBasePriceOfProducts();

    const discounts = this.getDiscounts().reduce(
      (total: number, cartItem: Discount) => total + cartItem.discountAmount,
      0
    );

    return basePrice - discounts;
  }

  public emptyCart(): void {
    this.setCart([]);
  }
}
