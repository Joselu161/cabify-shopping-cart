/**
 * Button component. Base implementation
 */
export interface Button {
  buttonText: string;
}
