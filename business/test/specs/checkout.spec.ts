import { Checkout } from "../../src";
import type { Discount } from "../../src/controllers/checkout.interfaces";
import {
  MOCK_AVAILABLE_PRODUCTS,
  MOCK_DISCOUNTS_TWO_PER_ONE_MUG,
  MOCK_DISCOUNTS_THREE_UNITS_CAP_TWO_PER_ONE_TSHIRT,
  MOCK_CUSTOM_DISCOUNT,
} from "../mocks/checkout.mock";

describe("checkout test suite", () => {
  let co: Checkout;

  describe("base functionallity of cart", () => {
    beforeEach(() => {
      co = new Checkout(
        MOCK_DISCOUNTS_TWO_PER_ONE_MUG,
        MOCK_AVAILABLE_PRODUCTS
      );
    });

    it("should return 0 if there are no products in the cart", () => {
      expect(co.getTotalAmountOfProducts()).toBe(0);
    });

    it("should return the quantity of each product and the total amount", () => {
      co.scan("TSHIRT").scan("CAP").scan("CAP").scan("TSHIRT").scan("CAP");

      expect(co.getQuantityOfProduct("CAP")).toBe(3);
      expect(co.getTotalAmountOfProducts()).toBe(5);
    });

    it("should reduce the quantity of the item in the cart if there are more than 2 items", () => {
      co.scan("TSHIRT").scan("CAP").scan("CAP").scan("TSHIRT").scan("CAP");

      expect(co.getQuantityOfProduct("CAP")).toBe(3);
      expect(co.getTotalAmountOfProducts()).toBe(5);

      co.removeItem("CAP");

      expect(co.getQuantityOfProduct("CAP")).toBe(2);
      expect(co.getTotalAmountOfProducts()).toBe(4);
    });
    it("should erase the product from the cart if there is only 1 item", () => {
      co.scan("CAP").scan("CAP").scan("TSHIRT").scan("CAP");

      expect(co.getQuantityOfProduct("TSHIRT")).toBe(1);
      expect(co.getTotalAmountOfProducts()).toBe(4);

      co.removeItem("TSHIRT");

      expect(co.getQuantityOfProduct("TSHIRT")).toBe(0);
      expect(co.getTotalAmountOfProducts()).toBe(3);
    });
    it("should increase the quantity of one item if receives positive amount", () => {
      co.scan("CAP").scan("CAP").scan("TSHIRT").scan("CAP");

      expect(co.getQuantityOfProduct("CAP")).toBe(3);

      co.changeQuantity("CAP", 5);
      expect(co.getQuantityOfProduct("CAP")).toBe(5);
    });

    it("should create a product that does not exist if receives positive amount", () => {
      co.scan("CAP").scan("CAP").scan("TSHIRT").scan("CAP");

      expect(co.getQuantityOfProduct("MUG")).toBe(0);

      co.changeQuantity("MUG", 3);
      expect(co.getQuantityOfProduct("MUG")).toBe(3);
    });

    it("should erase a product if receives negative amount or 0", () => {
      co.scan("CAP").scan("CAP").scan("TSHIRT").scan("CAP");

      expect(co.getQuantityOfProduct("CAP")).toBe(3);
      expect(co.getTotalAmountOfProducts()).toBe(4);

      co.changeQuantity("CAP", 0);
      expect(co.getQuantityOfProduct("CAP")).toBe(0);

      co.changeQuantity("TSHIRT", -2);
      expect(co.getQuantityOfProduct("TSHIRT")).toBe(0);
      expect(co.getTotalAmountOfProducts()).toBe(0);
    });

    it("should return the total price of the products", () => {
      co.scan("TSHIRT").scan("CAP").scan("TSHIRT").scan("MUG");

      expect(co.getTotalBasePriceOfProducts()).toBe(55);
    });
  });

  describe("discounts and total price", () => {
    it("should apply discount over MUG", () => {
      co = new Checkout(
        MOCK_DISCOUNTS_TWO_PER_ONE_MUG,
        MOCK_AVAILABLE_PRODUCTS
      );
      co.scan("CAP").scan("MUG").scan("MUG");

      const discount: Discount[] = [
        { discountAmount: 5, name: "2x1 Mug Offer" },
      ];

      expect(co.getDiscounts()).toEqual(discount);
    });

    it("should apply discount over CAP and TSHIRT", () => {
      co = new Checkout(
        MOCK_DISCOUNTS_THREE_UNITS_CAP_TWO_PER_ONE_TSHIRT,
        MOCK_AVAILABLE_PRODUCTS
      );
      co.scan("CAP").scan("CAP").scan("CAP").scan("TSHIRT").scan("TSHIRT");

      const discount: Discount[] = [
        { discountAmount: 1.5, name: "x3 Cap offer" },
        { discountAmount: 20, name: "2x1 TShirt Offer" },
      ];

      expect(co.getDiscounts()).toEqual(discount);
    });

    it("should apply customDiscount over TSHIRT", () => {
      co = new Checkout(MOCK_CUSTOM_DISCOUNT, MOCK_AVAILABLE_PRODUCTS);
      co.scan("TSHIRT");

      const discount: Discount[] = [
        { discountAmount: 4, name: "20% TShirt CUSTOM JL" },
      ];

      expect(co.getDiscounts()).toEqual(discount);
    });
  });

  describe("total price and total price with discount", () => {
    beforeEach(() => {
      co = new Checkout(
        MOCK_DISCOUNTS_THREE_UNITS_CAP_TWO_PER_ONE_TSHIRT,
        MOCK_AVAILABLE_PRODUCTS
      );
      co.scan("TSHIRT")
        .scan("TSHIRT")
        .scan("CAP")
        .scan("CAP")
        .scan("CAP")
        .scan("MUG");
    });

    it("should return the total price without discount (75)", () => {
      expect(co.getTotalBasePriceOfProducts()).toBe(75);
    });

    it("should return the total price with discount applied", () => {
      expect(co.total()).toBe(53.5);
    });

    it("should empty the cart", () => {
      co.emptyCart();

      expect(co.getTotalAmountOfProducts()).toBe(0);
    });
  });
});
