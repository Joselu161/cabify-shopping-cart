import { Component, Input, EventEmitter, Output } from '@angular/core';
import type { QuantityCounter } from 'business';
import { ChangeQuantityAction } from 'business';

@Component({
  selector: 'app-quantity-counter',
  templateUrl: './quantity-counter.component.html',
})
export class QuantityCounterComponent implements QuantityCounter {
  @Input() quantity!: number;
  @Output() changeQuantity = new EventEmitter<number>();

  public changeAction = ChangeQuantityAction;

  public handleQuantity(action: ChangeQuantityAction): void {
    switch (action) {
      case ChangeQuantityAction.INCREASE:
        this.quantity++;
        break;
      case ChangeQuantityAction.DECREASE:
        this.quantity > 0 && this.quantity--;
        break;

      default:
        break;
    }

    this.changeQuantity.emit(Number(this.quantity));
  }
}
