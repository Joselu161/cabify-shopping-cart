import type { ProductCode, ProductItem } from "../models";
import { ChangeQuantityAction } from "./checkout.interfaces";

export const updateQuantity = (
  action: ChangeQuantityAction,
  currentCuantity: number
) =>
  action === ChangeQuantityAction.INCREASE
    ? currentCuantity + 1
    : currentCuantity - 1;

export const changeQuantityOfProduct = (
  cart: ProductItem[],
  cartItemCode: ProductCode,
  handleQuantity: ChangeQuantityAction,
  newQuantity?: number
): ProductItem[] => {
  return cart.map((cartItem: ProductItem) => {
    return cartItem.code === cartItemCode
      ? {
          ...cartItem,
          quantity:
            handleQuantity === ChangeQuantityAction.CUSTOM
              ? newQuantity
              : updateQuantity(handleQuantity, cartItem.quantity),
        }
      : cartItem;
  });
};

export const removeProduct = (
  cart: ProductItem[],
  cartItemCode: ProductCode
) => {
  return [
    ...cart.filter((cartItem: ProductItem) => cartItem.code !== cartItemCode),
  ];
};

export const updatePrice = (product: ProductItem) => ({
  ...product,
  totalItemPrice: product.price * product.quantity,
});
