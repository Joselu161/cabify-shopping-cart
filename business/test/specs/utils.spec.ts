import { applyDiscountTwoPerOne, applyDiscountThreeUnits } from '../../src/utils/utils';

describe('applyDiscountTwoPerOne function', () => {
  test('should return 20 with 2 units which price is 20', () => {
    const result = applyDiscountTwoPerOne(2, 20);
    expect(result).toBe(20);
  });

  test('should return 40 with 3 units which price is 20', () => {
    const result = applyDiscountTwoPerOne(3, 20);
    expect(result).toBe(40);
  });

  test('should return 60 with 5 units which price is 20', () => {
    const result = applyDiscountTwoPerOne(5, 20);
    expect(result).toBe(60);
  });

  test('should return 60 with 6 units which price is 20', () => {
    const result = applyDiscountTwoPerOne(6, 20);
    expect(result).toBe(60);
  });
});

describe('applyDiscountThreeUnits function', () => {
  test('should not apply the discount if units are less than 3', () => {
    const result = applyDiscountThreeUnits(2, 10);
    expect(result).toBe(20);
  });

  test('should return 28.5 with 3 units which price is 10', () => {
    const result = applyDiscountThreeUnits(3, 10);
    expect(result).toBe(28.5);
  });

  test('should return 38 with two 4 which price is 10', () => {
    const result = applyDiscountThreeUnits(4, 10);
    expect(result).toBe(38);
  });
});
