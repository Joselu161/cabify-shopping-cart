# Cabify shopping cart challenge

#### _Jose Luis Aguilar Gómez_

_https://www.linkedin.com/in/jlaguilargomez/_

_https://github.com/jlaguilargomez_

### Running the app

First of all, to make it works, after downloading the project, you need to launch the command:

`npm run start`

It will create a local server to display the Angular app in the port http://localhost:4200/

![Shopping cart app](./example-jl.png?raw=true)

### Overall notes

I've created a repo with two main libraries: _angular_ and _business_ .

**Why?** Cause I wanted to split the _business_ logic from the _framework_ in order to make it independent, so you could use another framework that fits best for you.

In my case, I've been working last two years with Angular, so I've chosen this framework to create the app faster and be focused on the "business" logic.

In this case, Angular uses the other library to handle the logic of the app, by creating an instance of the main class (Checkout) and using the types defined on it.

### Business logic

Regarding the business logic, this package has been split into several folders to set the difference between the controllers, the models, and the views (MVC).

Component interfaces have been defined to use in the framework. They include all the event logic that should be used in each component.

All the logic implemented in the business folder has been tested with Jest, in order to check it, you just have to run `npm run test:business` in the root folder.

Going back to the logic, I've created a **Checkout** class, taking into account all the requisites attached to the challenge notes.

But, I've changed something.

You requested that this class should be instantiated with "pricingRules", but, in order to make it more reusable, I´ve added some _"improvements"_:

It must be instantiated with DISCOUNTS and AVAILABLE PRODUCTS,

- **DISCOUNTS** \_ You will be able to set up all the discounts that you want to include, just by config these properties: "name of discount, product in which this discount should be applied on, a function to implement the discount"

- **AVAILABLE_PRODUCTS** \_ It's necessary to inject a MAP in which there must be defined the products that the shop can sell.

Just by doing this, the cart is able to control all the products that you want to include and handle them with a proper discount.

This feature can be checked in the `checkout.spec.ts`, in which there have been tested several cases with different discounts.

### Angular

In this case, the framework creates a singleton of the Checkout class in the `checkout-cart.service` by injecting the products and discounts that we want to work with.
The instance is managed by the `cart-overview` page component, which allows the other "dumb" components to work with it too.

### Side notes

It may seem like a simple project, but it took a great effort from me, cause as I said, I've always been working with just an Angular repo, and I've never had created an Angular repo that consumes logic from another repo. But it has been fun!

Nevertheless, due to my work, I must say that I've not had too much time to focus on the challenge, and I've set aside the BONUS.
I wanted to focus on the logic part in order to, at least, learn something from this project.

And I must say I´ve learned a lot!

Indeed, it has been a challenge.
