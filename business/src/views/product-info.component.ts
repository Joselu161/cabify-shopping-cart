import type { ProductDetail } from "..";

/**
 * Display the info of the component: Image, name and code
 */
export interface ProductInfo {
  productInfo: ProductDetail;
}
