import type { Discount } from "../controllers/checkout.interfaces";
import type { ProductCode } from "../models/product-item";

/**
 * Component CartOverview. Base view to display cart
 */
export interface CartOverview {
  updateComponentProducts(): void;
  onCheckout(): void;
  onChangeProductQuantity({
    newQuantity,
    productCode,
  }: {
    newQuantity: number;
    productCode: ProductCode;
  }): void;
  totalAmountOfProducts: number;
  totalBasePriceOfProducts: number;
  discounts: Discount[];
  totalPriceWithDiscount: number;
}
